package fr.ulille.iutinfo.teletp;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;

    // TODO Q2.c
    private SuiviViewModel suivi;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        final String DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = DISTANCIEL;
        // TODO Q2.c
        suivi = new SuiviViewModel(this.getActivity().getApplication());
        // TODO Q4
        Spinner spinner = (Spinner) view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Spinner spinner2 = (Spinner) view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this.getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = parent.getItemAtPosition(position).toString();
                Update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView t = getActivity().findViewById(R.id.tvLogin);
            this.suivi.setUsername(t.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        this.Update();
        // TODO Q9
    }

    // TODO Q5.a
    private void Update(){
        Spinner spinner = (Spinner) getActivity().findViewById(R.id.spPoste);
        if (this.salle.equals("Distanciel")){
            spinner.setVisibility(View.INVISIBLE);
            spinner.setEnabled(false);
            this.suivi.setLocalisation("");
        }else{
            spinner.setVisibility(View.VISIBLE);
            spinner.setEnabled(true);
            this.suivi.setLocalisation(this.salle);
        }
    }

    // TODO Q9
}